module gitlab.com/teleport.media/gauth-client

go 1.14

require (
	github.com/cadyrov/godict v1.0.7
	github.com/cadyrov/goerr v1.0.12
)
