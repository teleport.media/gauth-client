package gauth

import (
	"net/http"
	"time"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

type UserRender struct {
	ID                int64                   `column:"id" json:"id"`
	Email             string                  `column:"email" json:"email"`
	Password          *string                 `column:"password" json:"-"`
	Name              *string                 `column:"name" json:"name"`
	Surname           *string                 `column:"surname" json:"surname"`
	Middlename        *string                 `column:"middlename" json:"middlename"`
	Phone             *string                 `column:"phone" json:"phone"`                 // Phone (e164)
	UserStatusID      int                     `column:"user_status_id" json:"userStatusId"` // User status (dictionary)
	PostalCode        *string                 `column:"postal_code" json:"postalCode"`
	CountryID         *int                    `column:"country_id" json:"countryId"` // Country (ISO 3166)
	City              *string                 `column:"city" json:"city"`
	Street            *string                 `column:"street" json:"street"`
	Housenum          *string                 `column:"housenum" json:"housenum"`
	Office            *string                 `column:"office" json:"office"`
	LanguageID        int                     `column:"language_id" json:"languageId"` // Language (ISO 639, ru/en)
	Roles             []string                `column:"roles" json:"roles"`
	Birthdate         *time.Time              `column:"birthdate" json:"birthdate"`
	PassportSeries    *string                 `column:"passport_series" json:"passportSeries"`
	PassportNumber    *string                 `column:"passport_number" json:"passportNumber"`
	PassportIssueDate *time.Time              `column:"passport_issue_date" json:"passportIssueDate"`
	PassportIssuer    *string                 `column:"passport_issuer" json:"passportIssuer"`
	CreatedAt         time.Time               `column:"created_at" json:"createdAt"`
	UpdatedAt         *time.Time              `column:"updated_at" json:"updatedAt"`
	BlockedAt         *time.Time              `column:"blocked_at" json:"blockedAt"`
	BlockedBy         *int64                  `column:"blocked_by" json:"blockedBy"`
	UserStatus        godict.DictionaryRender `json:"userStatus"`
	Language          godict.DictionaryRender `json:"language"`
	Country           godict.DictionaryRender `json:"country"`
}

type User struct {
	UserRender
	BlockedAt *time.Time `json:"-"`
	ID        int64      `json:"-"`
}

type UsersSearchForm struct {
	UsersIds      []int64  `json:"ids"`
	Limit         int      `json:"limit"`
	Page          int      `json:"page"`
	CountryIds    []int    `json:"countryIds"`
	UserStatusIds []int    `json:"userStatusIds"`
	Roles         []string `json:"roles"`
	Address       *string  `json:"address"`
	UserData      *string  `json:"userData"`
	IsDeleted     *bool    `json:"isDeleted"`
	Period
}

type Period struct {
	From *time.Time `json:"from"`
	To   *time.Time `json:"to"`
}

type AuthService struct {
	AuthConfig AuthConfig
	Cookie     []*http.Cookie
}

func NewAuthService(config AuthConfig, cookie []*http.Cookie) *AuthService {
	return &AuthService{config, cookie}
}

type AuthConfig struct {
	Bearer string `yml:"bearer"`
	Host   string `yml:"host"`
	Port   string `yml:"port"`
}

type LoginForm struct {
	Email    string `json:"login"`
	Password string `json:"password"`
}

type GoogleUserInfo struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email"`
	Picture       string `json:"picture"`
}

type GoogleUserInfoResponse struct {
	Message string          `json:"message"`
	Data    *GoogleUserInfo `json:"data"`
}

type OAuthLink struct {
	Link string `json:"link"`
}

type ConsentPageURLResponse struct {
	Data string `json:"data"`
}

type ErrorResponse struct {
	*goerr.AppError `json:"error"`
}

type UsersSearchResponse struct {
	Message    string            `json:"message"`
	Data       []UserRender      `json:"data"`
	Pagination godict.Pagination `json:"pagination"`
}

type ResponseUserRender struct {
	Message string      `json:"message"`
	Data    *UserRender `json:"data"`
}

type ResponseDictionaryRender struct {
	Message string            `json:"message"`
	Data    godict.Dictionary `json:"data"`
}

type LogoutResponse struct {
	Message string `json:"message"`
}

type RegistrationForm struct {
	ServiceURLForMailMessage string
	Email                    string `json:"email"`
}

type PasswordRecoveryForm struct {
	ServiceURLForMailMessage string
	Email                    string `json:"email"`
}

type PasswordRecoveryResponse struct {
	Message string `json:"message"`
	Data    string `json:"data"`
}

type ChangePasswordForm struct {
	UserID          int64
	Hash            string
	Password        string
	ConfirmPassword string
}

type ChangePasswordResponse struct {
	Message string      `json:"message"`
	Data    *UserRender `json:"data"`
}
