package gauth

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"mime/multipart"
	"net/http"
	"strconv"

	"github.com/cadyrov/godict"
	"github.com/cadyrov/goerr"
)

// region Registration

func (authService *AuthService) GoogleRegistrationRequest(callbackURL string) (*http.Response, goerr.IError) {
	URL := authService.baseURL() + "/v1/guest/registration/google?callbackUrl=" + callbackURL

	request, err := http.NewRequestWithContext(context.Background(), http.MethodGet, URL, &bytes.Buffer{})
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "*/*")

	defer request.Body.Close()

	response, err := sendRequest(request)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	authService.Cookie = append(authService.Cookie, response.Cookies()...)

	return response, nil
}

func (authService *AuthService) GoogleRegistrationCallback(request *http.Request) (*UserRender, goerr.IError) {
	newRequest, err := http.NewRequestWithContext(context.Background(),
		http.MethodGet, authService.baseURL()+"/v1/guest/registration/google/callback", &bytes.Buffer{})
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	newRequest.URL.RawQuery = request.URL.RawQuery
	newRequest.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)

	for _, cookie := range authService.Cookie {
		newRequest.AddCookie(cookie)
	}

	response, err := sendRequest(newRequest)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	defer request.Body.Close()
	defer newRequest.Body.Close()

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err := godict.ParseBody(response.Body, &jsonError); err != nil {
			return nil, goerr.New(err.Error())
		}

		return nil, jsonError.AppError
	}

	authService.Cookie = response.Cookies()

	render := ResponseUserRender{Data: &UserRender{}}

	if err := godict.ParseBody(response.Body, &render); err != nil {
		return nil, goerr.New(err.Error())
	}

	return render.Data, nil
}

func (authService *AuthService) GithubRegistrationRequest(callbackURL string) (*http.Response, goerr.IError) {
	URL := authService.baseURL() + "/v1/guest/registration/github?callbackUrl=" + callbackURL

	request, err := http.NewRequestWithContext(context.Background(), http.MethodGet, URL, &bytes.Buffer{})
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "*/*")

	defer request.Body.Close()

	response, err := sendRequest(request)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	authService.Cookie = append(authService.Cookie, response.Cookies()...)

	return response, nil
}

func (authService *AuthService) GithubRegistrationCallback(request *http.Request) (*UserRender, goerr.IError) {
	newRequest, err := http.NewRequestWithContext(context.Background(),
		http.MethodGet, authService.baseURL()+"/v1/guest/registration/github/callback", &bytes.Buffer{})
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	newRequest.URL.RawQuery = request.URL.RawQuery
	newRequest.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)

	for _, cookie := range authService.Cookie {
		newRequest.AddCookie(cookie)
	}

	response, err := sendRequest(newRequest)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	defer request.Body.Close()
	defer newRequest.Body.Close()

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err := godict.ParseBody(response.Body, &jsonError); err != nil {
			return nil, goerr.New(err.Error())
		}

		return nil, jsonError.AppError
	}

	authService.Cookie = response.Cookies()

	render := ResponseUserRender{Data: &UserRender{}}

	if err := godict.ParseBody(response.Body, &render); err != nil {
		return nil, goerr.New(err.Error())
	}

	return render.Data, nil
}

// endregion

// region Authorization

func (authService *AuthService) LoginRequest(loginForm LoginForm) (userRender *UserRender, e goerr.IError) {
	buffer := &bytes.Buffer{}

	multipartFormBody, err := writeMultipartLoginFormBody(buffer, loginForm)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	request, err := preparePostRequest(authService, buffer, multipartFormBody, "guest/auth")
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	response, err := sendRequest(request)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err := godict.ParseBody(response.Body, &jsonError); err != nil {
			e = goerr.New(err.Error())

			return
		}

		e = jsonError.AppError
	} else {
		authService.Cookie = response.Cookies()

		render := ResponseUserRender{Data: &UserRender{}}

		if err := godict.ParseBody(response.Body, &render); err != nil {
			e = goerr.New(err.Error())

			return
		}

		userRender = render.Data
	}

	return userRender, e
}

func writeMultipartLoginFormBody(buffer io.Writer, loginForm LoginForm) (multipartWriter *multipart.Writer,
	err goerr.IError) {
	multipartWriter = multipart.NewWriter(buffer)
	_ = multipartWriter.WriteField("email", loginForm.Email)
	_ = multipartWriter.WriteField("password", loginForm.Password)
	e := multipartWriter.Close()

	if e != nil {
		err = goerr.New(e.Error())

		return
	}

	return
}

func (authService *AuthService) GoogleOAuthLoginRequest(googleCallbackURL string) (*http.Response, goerr.IError) {
	buffer := &bytes.Buffer{}

	URL := authService.baseURL() + "/v1/guest/oauth/google/login?callbackUrl=" + googleCallbackURL

	request, err := http.NewRequestWithContext(context.Background(), http.MethodGet, URL, buffer)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "*/*")

	defer request.Body.Close()

	response, err := sendRequest(request)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	authService.Cookie = append(authService.Cookie, response.Cookies()...)

	return response, nil
}

func (authService *AuthService) GoogleOAuthCallback(request *http.Request) (userRender *UserRender, e goerr.IError) {
	newRequest, err := http.NewRequestWithContext(context.Background(),
		http.MethodGet, authService.baseURL()+"/v1/guest/oauth/google/callback", &bytes.Buffer{})
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	newRequest.URL.RawQuery = request.URL.RawQuery
	newRequest.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)

	for _, cookie := range authService.Cookie {
		newRequest.AddCookie(cookie)
	}

	response, err := sendRequest(newRequest)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	defer request.Body.Close()
	defer newRequest.Body.Close()

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err := godict.ParseBody(response.Body, &jsonError); err != nil {
			e = goerr.New(err.Error())

			return
		}

		e = jsonError.AppError

		return
	}

	authService.Cookie = response.Cookies()

	render := ResponseUserRender{Data: &UserRender{}}

	if err := godict.ParseBody(response.Body, &render); err != nil {
		e = goerr.New(err.Error())

		return
	}

	userRender = render.Data

	return userRender, e
}

func (authService *AuthService) GithubOAuthLoginRequest(callbackURL string) (*http.Response, goerr.IError) {
	buffer := &bytes.Buffer{}

	URL := authService.baseURL() + "/v1/guest/oauth/github/login?callbackUrl=" + callbackURL

	request, err := http.NewRequestWithContext(context.Background(), http.MethodGet, URL, buffer)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "*/*")

	defer request.Body.Close()

	response, err := sendRequest(request)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	authService.Cookie = append(authService.Cookie, response.Cookies()...)

	return response, nil
}

func (authService *AuthService) GithubOAuthCallback(request *http.Request) (userRender *UserRender, e goerr.IError) {
	newRequest, err := http.NewRequestWithContext(context.Background(),
		http.MethodGet, authService.baseURL()+"/v1/guest/oauth/github/callback", &bytes.Buffer{})
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	newRequest.URL.RawQuery = request.URL.RawQuery
	newRequest.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)

	for _, cookie := range authService.Cookie {
		newRequest.AddCookie(cookie)
	}

	response, err := sendRequest(newRequest)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	defer request.Body.Close()
	defer newRequest.Body.Close()

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err := godict.ParseBody(response.Body, &jsonError); err != nil {
			e = goerr.New(err.Error())

			return
		}

		e = jsonError.AppError

		return
	}

	authService.Cookie = response.Cookies()

	render := ResponseUserRender{Data: &UserRender{}}

	if err := godict.ParseBody(response.Body, &render); err != nil {
		e = goerr.New(err.Error())

		return
	}

	userRender = render.Data

	return userRender, e
}

func (authService *AuthService) LogoutRequest(cookie []*http.Cookie) (logoutResponse *LogoutResponse, e goerr.IError) {
	buffer := &bytes.Buffer{}

	request, err := http.NewRequestWithContext(context.Background(),
		http.MethodGet, authService.baseURL()+"/v1/users/logout", buffer)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	for _, cookie := range cookie {
		request.AddCookie(cookie)
	}

	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Accept", "*/*")

	response, err := sendRequest(request)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	if response != nil {
		defer response.Body.Close()
	}

	authService.Cookie = response.Cookies()
	logoutResponse = &LogoutResponse{}

	err = godict.ParseBody(response.Body, &logoutResponse)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	return logoutResponse, e
}

func (authService *AuthService) CheckRequest(requestCookie []*http.Cookie) (userRender *UserRender,
	cookie []*http.Cookie, e goerr.IError) {
	buffer := &bytes.Buffer{}

	request, err := http.NewRequestWithContext(context.Background(),
		http.MethodGet, authService.baseURL()+"/v1/guest/check", buffer)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	for _, cookie := range requestCookie {
		request.AddCookie(cookie)
	}

	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Accept", "*/*")

	response, err := sendRequest(request)
	if err != nil {
		return
	}

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err := godict.ParseBody(response.Body, &jsonError); err != nil {
			e = goerr.New(err.Error())

			return
		}

		e = jsonError.AppError
	} else {
		cookie = response.Cookies()
		render := ResponseUserRender{Data: &UserRender{}}
		if err := godict.ParseBody(response.Body, &render); err != nil {
			e = goerr.New(err.Error())

			return
		}
		userRender = render.Data
	}

	return userRender, cookie, e
}

func (authService *AuthService) PasswordRecoveryRequest(email string) (string, goerr.IError) {
	buffer := &bytes.Buffer{}
	multipartWriter := multipart.NewWriter(buffer)
	_ = multipartWriter.WriteField("email", email)

	err := multipartWriter.Close()
	if err != nil {
		return "", goerr.New(err.Error())
	}

	request, err := preparePostRequest(authService, buffer, multipartWriter, "guest/recovery-password")
	if err != nil {
		return "", goerr.New(err.Error())
	}

	for _, cookie := range request.Cookies() {
		request.AddCookie(cookie)
	}

	response, err := sendRequest(request)
	if err != nil {
		return "", goerr.New(err.Error())
	}

	if response != nil {
		defer response.Body.Close()
	}

	errorResponse := ErrorResponse{&goerr.AppError{}}

	passwordRecoveryResponse := PasswordRecoveryResponse{}

	if response.StatusCode != http.StatusOK {
		if err = godict.ParseBody(response.Body, &errorResponse); err != nil {
			return "", goerr.New(err.Error())
		}
	} else {
		if err = godict.ParseBody(response.Body, &passwordRecoveryResponse); err != nil {
			return "", goerr.New(err.Error())
		}
	}

	return passwordRecoveryResponse.Data, nil
}

func (authService *AuthService) PasswordChangeRequest(changePasswordForm ChangePasswordForm) (
	changePasswordResponse *ChangePasswordResponse, e goerr.IError) {
	buffer := &bytes.Buffer{}

	multipartFormBody, err := writeMultipartChangePasswordFormBody(buffer, changePasswordForm)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	request, err := preparePostRequest(authService, buffer, multipartFormBody, "guest/change-password")
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	response, err := sendRequest(request)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err := godict.ParseBody(response.Body, &jsonError); err != nil {
			e = goerr.New(err.Error())

			return
		}

		e = jsonError
	} else {
		jsonChangePasswordResponse := ChangePasswordResponse{}
		if err := godict.ParseBody(response.Body, &jsonChangePasswordResponse); err != nil {
			e = goerr.New(err.Error())

			return
		}
		changePasswordResponse = &jsonChangePasswordResponse

		return
	}

	return changePasswordResponse, e
}

func (authService *AuthService) Dictionaries(locale string) (dictionary *godict.Dictionary, e goerr.IError) {
	buffer := &bytes.Buffer{}

	request, err := http.NewRequestWithContext(context.Background(),
		http.MethodGet, authService.baseURL()+"/v1/dictionaries/"+locale, buffer)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Accept", "*/*")

	response, err := sendRequest(request)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err := godict.ParseBody(response.Body, &jsonError); err != nil {
			e = goerr.New(err.Error())

			return
		}

		e = jsonError.AppError
	} else {
		render := ResponseDictionaryRender{Data: godict.Dictionary{}}
		if err := godict.ParseBody(response.Body, &render); err != nil {
			e = goerr.New(err.Error())

			return
		}
		dictionary = &render.Data
	}

	return dictionary, e
}

// endregion

// region User

func (authService *AuthService) CreateUser(user User) (*UserRender, goerr.IError) {
	// Create request
	jsonBody, err := json.Marshal(user)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	request, err := http.NewRequestWithContext(context.Background(),
		http.MethodPost, authService.baseURL()+"/v1/users/create", bytes.NewBuffer(jsonBody))
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	defer request.Body.Close()
	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "*/*")

	response, err := sendRequest(request)
	if err != nil {
		return nil, goerr.New(err.Error())
	}

	if response != nil {
		defer response.Body.Close()
	}

	errorResponse := ErrorResponse{&goerr.AppError{}}

	responseUserRender := ResponseUserRender{Data: &UserRender{}}

	if response.StatusCode != http.StatusOK {
		if err = godict.ParseBody(response.Body, &errorResponse); err != nil {
			return nil, goerr.New(err.Error())
		}

		return nil, errorResponse
	} else {
		if err = godict.ParseBody(response.Body, &responseUserRender); err != nil {
			return nil, goerr.New(err.Error())
		}
	}

	return responseUserRender.Data, nil
}

func (authService *AuthService) UpdateUser(newUser User) (userRender *UserRender, e goerr.IError) {
	jsonBody, err := json.Marshal(newUser)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	request, err := http.NewRequestWithContext(context.Background(), http.MethodPut,
		authService.baseURL()+"/v1/users/"+strconv.FormatInt(newUser.ID, 10), bytes.NewBuffer(jsonBody))
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	defer request.Body.Close()

	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "*/*")

	response, err := sendRequest(request)
	if err != nil {
		return
	}

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err = godict.ParseBody(response.Body, &jsonError); err != nil {
			e = goerr.New(err.Error())

			return
		}

		e = jsonError.AppError
	} else {
		responseRender := ResponseUserRender{Data: &UserRender{}}
		if err = godict.ParseBody(response.Body, &responseRender); err != nil {
			e = goerr.New(err.Error())

			return
		}
		userRender = responseRender.Data
	}

	return userRender, e
}

func (authService *AuthService) Find(userSearchForm *UsersSearchForm) (
	userRenders []UserRender, p godict.Pagination, e goerr.IError) {
	jsonBody, err := json.Marshal(userSearchForm)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	request, err := http.NewRequestWithContext(context.Background(),
		http.MethodPost, authService.baseURL()+"/v1/users", bytes.NewBuffer(jsonBody))
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	defer request.Body.Close()
	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "*/*")

	response, err := sendRequest(request)
	if err != nil {
		return
	}

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err = godict.ParseBody(response.Body, &jsonError); err != nil {
			e = goerr.New(err.Error())

			return
		}

		e = jsonError.AppError
	} else {
		renders := UsersSearchResponse{Data: make([]UserRender, 0)}
		if err = godict.ParseBody(response.Body, &renders); err != nil {
			e = goerr.New(err.Error())

			return
		}
		userRenders = renders.Data
		p = renders.Pagination
	}

	return userRenders, p, e
}

func (authService *AuthService) GetAllUsers(searchForm *UsersSearchForm) (userRenders []UserRender, e goerr.IError) {
	jsonBody, err := json.Marshal(searchForm)
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	request, err := http.NewRequestWithContext(context.Background(),
		http.MethodPost, authService.baseURL()+"/v1/users", bytes.NewBuffer(jsonBody))
	if err != nil {
		e = goerr.New(err.Error())

		return
	}

	defer request.Body.Close()
	request.Header.Add("Authorization", "Bearer "+authService.AuthConfig.Bearer)
	request.Header.Add("Content-Type", "application/json")
	request.Header.Add("Accept", "*/*")

	response, err := sendRequest(request)
	if err != nil {
		return
	}

	if response != nil {
		defer response.Body.Close()
	}

	if response.StatusCode != http.StatusOK {
		jsonError := ErrorResponse{&goerr.AppError{}}
		if err = godict.ParseBody(response.Body, &jsonError); err != nil {
			e = goerr.New(err.Error())

			return
		}

		e = jsonError.AppError
	} else {
		renders := UsersSearchResponse{Data: make([]UserRender, 0)}
		if err = godict.ParseBody(response.Body, &renders); err != nil {
			e = goerr.New(err.Error())

			return
		}
		userRenders = renders.Data
	}

	return userRenders, e
}

// endregion

// region Common

func (authService *AuthService) baseURL() string {
	return authService.AuthConfig.Host + authService.AuthConfig.Port + "/api"
}

func sendRequest(request *http.Request) (response *http.Response, err goerr.IError) {
	client := &http.Client{}

	response, e := client.Do(request)
	if e != nil {
		err = goerr.New(e.Error())

		return
	}

	return
}

func writeMultipartChangePasswordFormBody(buffer io.Writer, changePasswordForm ChangePasswordForm) (
	multipartWriter *multipart.Writer, err goerr.IError) {
	multipartWriter = multipart.NewWriter(buffer)
	_ = multipartWriter.WriteField("hash", changePasswordForm.Hash)
	_ = multipartWriter.WriteField("password", changePasswordForm.Password)
	_ = multipartWriter.WriteField("confirmPassword", changePasswordForm.ConfirmPassword)

	e := multipartWriter.Close()
	if e != nil {
		err = goerr.New(e.Error())

		return
	}

	return
}

func preparePostRequest(authServices *AuthService, buffer io.Reader, multipartFormBody *multipart.Writer,
	urlPath string) (request *http.Request, err goerr.IError) {
	url := authServices.baseURL() + "/v1/" + urlPath

	request, e := http.NewRequestWithContext(context.Background(), http.MethodPost, url, buffer)
	if e != nil {
		err = goerr.New(e.Error())

		return
	}

	for _, cookie := range authServices.Cookie {
		request.AddCookie(cookie)
	}

	request.Header.Set("Content-Type", multipartFormBody.FormDataContentType())
	request.Header.Add("Authorization", "Bearer "+authServices.AuthConfig.Bearer)
	request.Header.Add("Accept", "*/*")

	return
}

// endregion
